import { createContext } from 'react';
// Un contex è una funzione che possimo condividere tra i component senza usare il props drilling
export const AuthContext = createContext({
    isLoggedIn: false,
    userId: null,
    token: null,
    login: () => {},
    logout: () => {}
  });