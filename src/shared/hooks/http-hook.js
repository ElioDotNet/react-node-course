import { useState, useCallback, useRef, useEffect } from 'react';

const useHttpClient = () => {
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState();

  const activeHttpRequests = useRef([]); // essendo un riferimento non cambia quando viene reindirizzato

  const sendRequest = useCallback(async ( // useCallback si evita di rieseguire il codice più volte
    url,
    method = 'GET', // La fetch di default usa una GET
    body = null,
    headers = {}
  ) => {
    setIsLoading(true);
    // AbortController serve per cancellare una chiamata fatta con una Fetch
    const httpAbortCtrl = new AbortController();
    activeHttpRequests.current.push(httpAbortCtrl);

    try {
      // Anzichè usare librerie esterne come Axios si può usare la fetch fornita dal browser
      const response = await fetch(url, {
        method,
        body,
        headers,
        signal: httpAbortCtrl.signal // ==> specifica quale fetch si vuole cancellare(se cancellata genera errore e si va nel catch)
      });

      const responseData = await response.json(); // trasformiamo la response in json

      // Si vuole pulire i controlli che appartengono alla request che sono aborted
      activeHttpRequests.current = activeHttpRequests.current.filter(
        reqCtrl => reqCtrl !== httpAbortCtrl
      );

      // La response di una fetch con ok corrisponse a codice 200, diversamente 4xx o 5xx
      if (!response.ok) {
        throw new Error(responseData.message); // si va nel catch
      }

      setIsLoading(false);
      return responseData;

    } catch (err) {
      setError(err.message);
      setIsLoading(false);
      throw err; // occorre restituire l'errore al chiamante
    }

  }, []); // nessuna dipendenza

  const clearError = () => {
    setError(null);
  };

  // In questo caso useEffect si usa come componentWillUnmount nel class component
  useEffect(() => {
    return () => { // clean up va dopo la return () =>
      // eslint-disable-next-line react-hooks/exhaustive-deps
      activeHttpRequests.current.forEach(abortCtrl => abortCtrl.abort()); // con abort si annulla la chiamata
    };
  }, []);

  return { isLoading, error, sendRequest, clearError };
};

export default useHttpClient;
