import { useCallback, useEffect, useState } from 'react';

let logoutTimer;

export const useAuth = () => {
  const [tokenExpirationDate, SetTokenExpirationDate] = useState();
  const [token, setToken] = useState(null);
  const [userId, setUserId] = useState(null);

  const login = useCallback((uid, token, expirationDate) => {
    setToken(token);
    setUserId(uid);
    // Se non si ha una scadenza token, si crea una variabile di scadenza token di un'ora
    const tokenExpirationDate =
      expirationDate || new Date(new Date().getTime() + 1000 * 60 * 60);
    SetTokenExpirationDate(tokenExpirationDate);
    localStorage.setItem(
      'userData',
      JSON.stringify({
        userId: uid,
        token: token,
        expiration: tokenExpirationDate.toISOString(),
      })
    );
  }, []);

  const logout = useCallback(() => {
    setToken(null);
    SetTokenExpirationDate(null);
    setUserId(null);
    localStorage.removeItem('userData');
  }, []);

  useEffect(() => {
    if (token && tokenExpirationDate) {
      // Tempo rimanente alla scadenza del token
      const remainingTime =
        tokenExpirationDate.getTime() - new Date().getTime(); // con getTime ritorna il valore in millisecondi
      logoutTimer = setTimeout(logout, remainingTime); // restituisce un id
    } else {
      clearTimeout(logoutTimer); // l'id viene cancellato
    }
  }, [token, logout, tokenExpirationDate]);

  // Prima viene eseguito il render del component e DOPO useEffect
  useEffect(() => {
    const storedData = JSON.parse(localStorage.getItem('userData'));
    if (
      storedData &&
      storedData.token &&
      new Date(storedData.expiration) > new Date() // il token ha una scadenza nel futuro ed è ancora valido
    ) {
      login(
        storedData.userId,
        storedData.token,
        new Date(storedData.expiration)
      );
    }
  }, [login]); // essendo dentro una useCallback viene eseguita una volta sola

  return { token, userId, login, logout }
};
