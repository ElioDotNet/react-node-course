import React, { useContext } from 'react';
import { NavLink } from 'react-router-dom';

import './NavLinks.css';
import { AuthContext } from "../../context/auth-context";

const NavLinks = () => {
    const auth = useContext(AuthContext); // possiamo utilizzare quello che ci arriva dal context

    return <ul className="nav-links">
        <li>
            <NavLink to="/" exact>ALL USERS</NavLink>
        </li>
        {auth.isLoggedIn && (
        <li>
            <NavLink to={`/${auth.userId}/places`}>MY PLACES</NavLink>
        </li>
        )}
        {auth.isLoggedIn && (
        <li>
            <NavLink to="/places/new">ADD PLACE</NavLink>
        </li>
        )}
        {!auth.isLoggedIn && (
        <li>
            <NavLink to="/auth">AUTHENTICATE</NavLink>
        </li>
        )}
        {auth.isLoggedIn && (
            <button onClick={auth.logout}>LOGOUT</button>
        )}
    </ul>
};

export default NavLinks;
