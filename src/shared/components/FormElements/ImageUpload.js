import React, { useRef, useState, useEffect } from 'react';

import Button from './Button';

import './ImageUpload.css';

const ImageUpload = props => {
  const filePickerRef = useRef();
  const [file, setFile] = useState();
  const [previewUrl, setPreviewUrl] = useState();
  const [isValid, setIsValid] = useState(false);

  const pickImageHandler = () => {
    filePickerRef.current.click(); // simula l'vento click dell'input al click del button
  };

  // Viene generato tutte le volche che si fa un upload di un file
  useEffect(() => {
    if (!file) {
      return;
    }
    const fileReader = new FileReader();
    fileReader.onload = () => {
      // questa funzione viene eseguita quando è termitata la readAsDataURL
      setPreviewUrl(fileReader.result);
    };
    fileReader.readAsDataURL(file); // => genera fileReader.onload()
  }, [file]);

  const pickedHandler = event => {
    let pickedFile;
    // setIsValid non cambia immediatamente il valore, quindi si usa questa variabile
    let fileIsValid = isValid;
    if (event.target.files && event.target.files.length === 1) {
      // il file esiste e ne ho selezionato solo uno
      pickedFile = event.target.files[0];
      setFile(pickedFile);
      setIsValid(true);
      fileIsValid = true;
    } else {
      setIsValid(false);
      fileIsValid = false;
    }
    props.onInput(props.id, pickedFile, fileIsValid); // genero un evento al chiamante con id, file selezionato e se valido
  };

  return (
    <div className="form-control">
      <input
        id={props.id}
        ref={filePickerRef}
        style={{ display: 'none' }}
        type="file"
        accept=".jpg,.png,.jpeg"
        onChange={pickedHandler}
      />
      <div className={`image-upload ${props.center && 'center'}`}>
        <div className="image-upload__preview">
          {previewUrl && <img src={previewUrl} alt="Preview" />}
          {!previewUrl && <p>Please pick an image.</p>}
        </div>
        <Button type="button" onClick={pickImageHandler}>
          PICK IMAGE
        </Button>
      </div>
      {!isValid && <p>{props.errorText}</p>}
    </div>
  );
};

export default ImageUpload;
