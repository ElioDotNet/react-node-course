import React, { lazy, Suspense } from 'react';
import {
  BrowserRouter as Router,
  Route,
  Redirect,
  Switch,
} from 'react-router-dom';

// import Auth from './user/pages/Auth';
// import Users from './user/pages/Users';
// import NewPlace from './places/pages/NewPlace';
// import UserPlaces from './places/pages/UserPlaces';
// import UpdatePlace from './places/pages/UpdatePlace';

import MainNavigation from './shared/components/Navigation/MainNavigation';

import { AuthContext } from './shared/context/auth-context';
import { useAuth } from './shared/hooks/auth.hook';
import LoadingSpinner from './shared/components/UIElements/LoadingSpinner';

// GESTIONE LAZY LOADING - I component vengono letti solo quando sono richiesti
const Users = lazy(() => import('./user/pages/Users'));
const NewPlace = lazy(() => import('./places/pages/NewPlace'));
const UpdatePlace = lazy(() => import('./places/pages/UpdatePlace'));
const UserPlaces = lazy(() => import('./places/pages/UserPlaces'));
const Auth = lazy(() => import('./user/pages/Auth'));

const App = () => {
  const { token, userId, login, logout } = useAuth();

  let routes;

  if (token) {
    routes = (
      <Switch>
        <Route path='/' exact>
          <Users />
        </Route>
        <Route path='/:userId/places' exact>
          <UserPlaces />
        </Route>
        <Route path='/places/new' exact>
          <NewPlace />
        </Route>
        <Route path='/places/:placeId'>
          <UpdatePlace />
        </Route>
        <Redirect to='/' />
      </Switch>
    );
  } else {
    routes = (
      <Switch>
        <Route path='/' exact>
          <Users />
        </Route>
        <Route path='/:userId/places' exact>
          <UserPlaces />
        </Route>
        <Route path='/auth'>
          <Auth />
        </Route>
        <Redirect to='/auth' />
      </Switch>
    );
  }

  return (
    // Tutte le volte che i valori cambiano il provider passa il nuovo valore a tutti i componenti della gerarchia
    <AuthContext.Provider
      value={{
        isLoggedIn: !!token, // se esiste il token, il login è andato a buon fine
        token: token,
        login: login,
        logout: logout,
        userId,
      }}
    >
      <Router>
        <MainNavigation />
        <main>
          {/*  Suspense si usa per il Lazy Loding */}
          {/*  fallback si usa quando lazy loading del route ci mette troppo tempo */}
          <Suspense
            fallback={
              <div className='center'>
                <LoadingSpinner />
              </div>
            }
          >
            {routes}
          </Suspense>
        </main>
      </Router>
    </AuthContext.Provider>
  );
};

export default App;
