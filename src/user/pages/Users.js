import React, { useEffect, useState, Fragment } from 'react';

import UsersList from '../components/UsersList';
import ErrorModal from '../../shared/components/UIElements/ErrorModal';
import LoadingSpinner from '../../shared/components/UIElements/LoadingSpinner';
import useHttpClient from '../../shared/hooks/http-hook';

const Users = () => {
  const [loadedUsers, setLoadedUsers] = useState();
  const { isLoading, error, sendRequest, clearError } = useHttpClient();

  useEffect(() => { // async si potrebbe usare direttamente in useEffect ma è sconsigliato
    // Si crea una funzione autoeseguibile
    const fetchUsers = async () => {
      await sendRequest(process.env.REACT_APP_BACKEND_URL + '/users') // REACT_APP_BACKEND_URL => variabile nel file .env
        .then(
          responseData => setLoadedUsers(responseData.users) // elenco degli users
        )
        .catch(err => console.log(err));
    };
    fetchUsers();
  }, [sendRequest]);

  return (
    <Fragment>
      <ErrorModal error={error} onClear={clearError} />
      {isLoading && (
        <div className='center'>
          <LoadingSpinner />
        </div>
      )}
      {!isLoading && loadedUsers && <UsersList items={loadedUsers} />}
    </Fragment>
  );
};

export default Users;
