import React, { useState, useContext, Fragment } from 'react';

import './Auth.css';

import {
  VALIDATOR_EMAIL,
  VALIDATOR_MINLENGTH,
  VALIDATOR_REQUIRE
} from '../../shared/util/validators';

import useHttpClient from '../../shared/hooks/http-hook';
import useForm from '../../shared/hooks/form-hook';
import Button from '../../shared/components/FormElements/Button';
import { AuthContext } from '../../shared/context/auth-context';
import Card from '../../shared/components/UIElements/Card';
import ErrorModal from '../../shared/components/UIElements/ErrorModal';
import LoadingSpinner from '../../shared/components/UIElements/LoadingSpinner';
import Input from '../../shared/components/FormElements/Input';
import ImageUpload from '../../shared/components/FormElements/ImageUpload';

const Auth = () => {
  const auth = useContext(AuthContext);
  const [isLoginMode, setIsLoginMode] = useState(true);
  const { isLoading, error, sendRequest, clearError } = useHttpClient();

  const [formState, inputHandler, setFormData] = useForm(
    {
      email: {
        value: '',
        isValid: false
      },
      password: {
        value: '',
        isValid: false
      }
    },
    false
  );

  const authSubmitHandler = async event => {
    event.preventDefault();
    if (isLoginMode) {
      // LOGIN
      await sendRequest(
        process.env.REACT_APP_BACKEND_URL + '/users/login', // REACT_APP_BACKEND_URL => variabile nel file .env
        'POST',
        JSON.stringify({
          email: formState.inputs.email.value,
          password: formState.inputs.password.value
        }),
        {
          'Content-Type': 'application/json'
        }
      )
        .then(responseData => {
          // Si può usare una then perchè l'errore viene gestito nell'hook
          auth.login(responseData.userId, responseData.token); // login con token solo se NON ci sono errori
        })
        .catch(err => console.log(err));
    } else {
      // SIGNUP
      // FormData => viene usato per gestire anche dati in formato binario
      const formData = new FormData();
      formData.append('email', formState.inputs.email.value);
      formData.append('name', formState.inputs.name.value);
      formData.append('password', formState.inputs.password.value);
      formData.append('image', formState.inputs.image.value); // 'image' è la chiave usata nel backend
      await sendRequest(
        process.env.REACT_APP_BACKEND_URL + '/users/signup', // REACT_APP_BACKEND_URL => variabile nel file .env
        'POST',
        formData // usando FormData non occorre più specificare l'Header
        // {
        //   'Content-Type': 'application/json'
        // },
      )
        .then(responseData => {
          // si può usare una then perchè l'errore viene gestito nell'hook
          auth.login(responseData.userId, responseData.token); // login solo se NON ci sono errori
        })
        .catch(err => console.log(err));
    }
  };

  const switchModeHandler = () => {
    if (!isLoginMode) {
      // Sign Up Mode: ci interessa solo mail e password
      setFormData(
        {
          ...formState.inputs,
          name: undefined,
          image: undefined
        },
        formState.inputs.email.isValid && formState.inputs.password.isValid
      );
    } else {
      // Login Mode: ci interessa mail, password e name
      setFormData(
        {
          ...formState.inputs,
          name: {
            value: '',
            isValid: false
          },
          image: {
            value: null,
            isValid: false
          }
        },
        false
      );
    }
    setIsLoginMode(prevMode => !prevMode); // inverte il loginMode
  };

  return (
    <Fragment>
      <ErrorModal error={error} onClear={clearError} />
      <Card className='authentication'>
        {isLoading && <LoadingSpinner asOverlay />}
        <h2>Login Required</h2>
        <hr />
        <form onSubmit={authSubmitHandler}>
          {!isLoginMode && (
            <Input
              element='input'
              id='name'
              type='text'
              label='Your Name'
              validators={[VALIDATOR_REQUIRE()]}
              errorText='Please enter a name.'
              onInput={inputHandler}
            />
          )}
          {/* Possiamo usare inputHandler perchè ha gli stessi parametri di ciò che viene restituito */}
          {!isLoginMode && (
            <ImageUpload
              center
              id='image'
              onInput={inputHandler}
              errorText='Please provide an image'
            />
          )}
          <Input
            element='input'
            id='email'
            type='email'
            label='E-Mail'
            validators={[VALIDATOR_EMAIL()]}
            errorText='Please enter a valid email address.'
            onInput={inputHandler}
          />
          <Input
            element='input'
            id='password'
            type='password'
            label='Password'
            validators={[VALIDATOR_MINLENGTH(6)]}
            errorText='Please enter a valid password, at least 6 characters.'
            onInput={inputHandler}
          />
          <Button type='submit' disabled={!formState.isValid}>
            {isLoginMode ? 'LOGIN' : 'SIGNUP'}
          </Button>
        </form>
        <Button inverse onClick={switchModeHandler}>
          SWITCH TO {isLoginMode ? 'SIGNUP' : 'LOGIN'}
        </Button>
      </Card>
    </Fragment>
  );
};

export default Auth;
